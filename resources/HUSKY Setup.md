[TOC]

# HUSKY Setup

## Safety Inspection

1. Inspect for loose connections on HUSKY, tighten the connections if present or remove if not required.
2. Inspect for undesirable obstacles, remove them if present.

## Turning On HUSKY

1. Remove the battery door and connect the battery power cable into the MCU.
2. Turn on the Husky.
3. Release all emergency stop controls on the Husky. The ”COMM” light should illuminate green.
4. Use the gamepad to control Husky’s motion.

![1549084951528](C:\Users\VINEET_NEW\AppData\Roaming\Typora\typora-user-images\1549084951528.png)

## Network Configuration HUSKY

| **PARAMETER**               | **VALUE**       |
| :-------------------------- | :-------------- |
| Robot Static IP             | 192.168.131.1   |
| Robot Hostname              | cpr-umb02       |
| Robot Username              | administrator   |
| Robot Password              | clearpath       |
| Serial Number               | a200-0483       |
| ARK IP                      | 192.168.132.111 |
| Front Hokuyo UST10LX IP     | 192.168.132.21  |
| Rear Hokuyo UST10LX IP      | 192.168.132.22  |
| SwiftNav IP                 | 192.168.0.222   |
| Access Point Radio IP       | 192.168.131.51  |
| Access Point Radio Username | admin           |
| Access Point Radio Password | clearpath       |
| Access Point SSID           | UMB02 Husky     |

## Backup HUSKY

**Kindly note backup the husky on the very first use and after very use**

1. As a fail-safe, please make an image of your robot's hard drive. You should always be able to restore this image if you need to revert back to your previous configuration.

   1. The easiest approach may be to either connect a removable (USB or similar) hard drive to the robot PC, or to unplug the robot hard drive and insert it into a PC or workstation.
   2. You can then use a tool such as [CloneZilla](http://clonezilla.org/show-live-doc-content.php?topic=clonezilla-live/doc/01_Save_disk_image) or [dd](https://wiki.archlinux.org/index.php/disk_cloning) to write a backup image of your robot's hard drive onto another hard drive.
   3. Alternatively, you can simply replace the robot computer's hard-drive, reserving the Hydro-configured drive and installing a new one to use with Indigo.

2. There are several places in the filesystem you should specifically look for customizations for your Husky:

   | **Location**:                        | **Description**:                                             |
   | ------------------------------------ | ------------------------------------------------------------ |
   | /etc/network/interfaces              | Your robot may have a custom network configuration configured in this file. |
   | /etc/ros/hydro/husky-core.d/*.launch | Will contain base.launch and description.launch, may contain custom launch files for your robot configuration. |
   | /etc/ros/setup.bash                  | May contain environment variables for your configuration.    |

## Communicating With HUSKY

To communicate directly with the robot PC, you can SSH in. It will be necessary to ssh into the robot for tasks such as installing, modifying or removing software and files on the robot. Note that you will not be able to use GUI tools such as rviz over an SSH connection:

```
ssh administrator@192.168.131.1
```

OR

```
ssh administrator@cpr-umb02
```

In order to use rviz and other visualization tools, you must declare the robot as master, and set the user computerIP. In a console on the user pc, type: 

```
export ROS_MASTER_URI=http://cpr-umb02:11311
```

You should then be able to view a list of topics published by the robot with:

```
rostopic list
```

It will be necessary to declare the robot as master in every new terminal window, unless you change the master permanently in your ROS environment variables. If you are unable to connect with the robot via its hostname, your computer or network equipment may not be routing hostnames properly. In Ubuntu on your local computer, open your /etc/hosts file:

```
sudo nano /etc/hosts
```

Add the following line immediately below the line that contains 127.0.1.1, substituting in the robot’s current
wifi IP address. This address may be obtained by connecting directly to the robot via Ethernet, and using the “ifconfig” command. You may want to talk to your system administrator about giving the robot a permanent wifi address to ensure it always connects with the same IP address. The below example shows the setting if wired directly into the robot Lan.

```
192.168.131.1 			cpr-umb02
```

To ease communications between the robot and your computer, you can also add a similar entry in the robot’s computer, pointing at one or more development computers.

## Setting Up Your Workspace

There is a workspace installed in your robot for development and custom packages. You will likely want to be able to use these packages on your local machine. To do this, make a new folder on your local computer:

```
mkdir catkin_ws && cd catkin_ws
```

```
mkdir src && cd src
```

Then copy all of the packages from the robot to your local machine

```
scp -r administrator@cpr-umb02:~/catkin_ws/src/* .
```

```
cd ..
```

Now, make sure you have all of the dependencies needed to build these packages:

```
rosdep install -- from-paths src -- ignore-src -- rosdistro =$ROS_DISTRO -i -y
```

```
catkin_make
```

```
source ~/catkin_ws/devel/setup.bash
```

## GPU

The computer in your robot has been equipped with a Graphics Processing Unit (GPU). Should you need to connect a monitor to the robot computer, make sure it is connected to one of the ports on the GPU. To check whether the GPU is functioning properly, you can use the nvidia-smi utility:

```
nvidia-smi
```

The current driver version of your card should be listed in the top row.

# Setting Up WAP 

The wireless access point built into your robot is a convenient way to communicate with your robot without requiring a connection to wifi infrastructure in your location. Simply turn on the robot, and connect to the SSID being broadcast by the robot’s access point. The SSID and login information are available in the Network Information section of this document.

Once connected to your robot’s access point, your computer will be granted an IP address via DHCP. It should now be possible to communicate with the robot’s onboard computer and payloads.

Should you need to change any settings on the AP, connect to it as usual via wifi. Then, enter its IP address into a web browser on your computer.

To connect your robot to the internet, plug a live internet connection into the WAN port on the access point. The robot and any connected computers should then be granted access to the internet. 

Some corporate network infrastructures may not permit an unauthorized access point. If this is the case at your location, please contact your IT administrator for assistance with connecting the Base Station to your intranet.

# ARK Control

Start the ARK Bridge. SSH into the robot, and enter:

```
roslaunch husky_cpr_ark_navigation husky_ark_navigation.launch
```

Check Diagnostics. On your local machine,

```
rosrun rqt_robot_monitor rqt_robot_monitor
```

Under ”Other,” the various ARK Bridge topics should display green.

Start ARK. On the robot,

```
rosservice call /ark_bridge/start_autonomy ”req_data: {}”
```

The command should complete with the status ”ark_service_timeout: False”. If the status is True, then an error has occurred at some point.

Use the ARK. On your local machine, connected to the same network as the robot, open a Google Chrome browser to:

```
http://192.168.131.1
```

Please see the included ARK Manual for further information on using the ARK interface and its various subsystems.

*Your computer has been configured with a dedicated Ethernet port for ARK communications. When making changes to your robot or computer, make sure that the ARK remains connected to this port, and the network settings remain unchanged. If you need to make changes to your robot that require modifying this configuration, please contact Clearpath Support.*

# Setting Up Sensors

Instructions for setting up and reading from sensors on the HUSKY.

## Bumblebee Camera

The Bumblebee camera images cannot be viewed via ssh. Declare the robot as ROS Master, then from the command line:

```
rosrun image_view image_view image:=/camera/left/image_color compressed
rosrun image_view image_view image:=/camera/right/image_color compressed
```

The Bumblebee image streams may also be viewed from within rviz by clicking ”Add,” selecting the ”By Topic” tab, then choosing the desired ”/image” stream (NOT /camera). 

*To calibrate your camera, use the camera_calibration package: http://wiki.ros.org/camera_calibration*

## HOKUYO UTM-10LX

The Hokuyo UTM-10LX is an Ethernet-connected single-beam LIDAR. The data it produces is best viewed from within rviz, but you can check that it is publishing data using the hz command:

```
rostopic hz /front/scan
```

```
rostopic hz /rear/scan
```

The Hokuyo LIDAR data may be easily added to rviz, by navigating to the ”Sensing” sub-folder and adding the relevant Hokuyo topic to ”LaserScan.”

## MICROSTRAIN GX5-25 IMU

The Microstrain IMU publishes filtered data. This imu data may be checked by echoing the following topics:

```
rostopic echo /imu/data
```

## SWIFTNAV GPS

The SwiftNav receiver features a number of status LEDs which can be used to tell at a glance the status of the sensor.

| Light Indication | Meaning             |
| ---------------- | ------------------- |
| Solid Green      | Power On            |
| Flashing Yellow  | Waiting for GPS Fix |
| Solid Yellow     | GPS Fix Achieved    |

The SwiftNav GPS publishes to /piksi/navsatfix_best_fix. Note that the topic will only publish when a valid GPS fix has been achieved:

```
rostopic echo/piksi/navsatfix_best_fix
```

Additional sensor data from the SwiftNav GPS receiver is available from other topics in the same namespace, such as:

```
rostopic echo/piksi/tracking_state
rostopic echo/piksi/debug/receiver_state
```

# VISUALIZING IN RVIZ

You can visualize your robot using rviz. To do so, you must first move a copy of any extra customizations from the robot into a workspace on your local computer (See ”Setting Up Your Workspace.”) Then, source the workspace:

Declare the robot as ROS Master:

```
export ROS_MASTER_URI=http://cpr-umb02:11311
```

Launch rviz:

```
roslaunch husky_viz view_robot.launch
```

![1549296640187](C:\Users\VINEET_NEW\AppData\Roaming\Typora\typora-user-images\1549296640187.png)

You can rotate the model using your cursor, and zoom in or out by scrolling up or down. Strafe by holding down Shift and dragging the model. The robot itself may be driven directly from rviz by changing to interact mode. In this mode, arrows appear around the model. Drag the arrows to make the robot move. 

Additional sensor topics may be added to the rviz interface by clicking the ”Add” button in the bottom left, selecting ”By topic,” then choosing the desired topic from the list.