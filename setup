# HUSKY Setup

## Safety Inspection

1. Inspect for loose connections on HUSKY, tighten the connections if present or remove if not required.
2. Inspect for undesirable obstacles, remove them if present.

## Turning On HUSKY

1. Remove the battery door and connect the battery power cable into the MCU.
2. Turn on the Husky.
3. Release all emergency stop controls on the Husky. The ”COMM” light should illuminate green.
4. Use the gamepad to control Husky’s motion.

![1549084951528](C:\Users\VINEET_NEW\AppData\Roaming\Typora\typora-user-images\1549084951528.png)

## Network Configuration HUSKY

| **PARAMETER**               | **VALUE**       |
| :-------------------------- | :-------------- |
| Robot Static IP             | 192.168.131.1   |
| Robot Hostname              | cpr-umb02       |
| Robot Username              | administrator   |
| Robot Password              | clearpath       |
| Serial Number               | a200-0483       |
| ARK IP                      | 192.168.132.111 |
| Front Hokuyo UST10LX IP     | 192.168.132.21  |
| Rear Hokuyo UST10LX IP      | 192.168.132.22  |
| SwiftNav IP                 | 192.168.0.222   |
| Access Point Radio IP       | 192.168.131.51  |
| Access Point Radio Username | admin           |
| Access Point Radio Password | clearpath       |
| Access Point SSID           | UMB02 Husky     |

## Backup HUSKY

**Kindly note backup the husky on the very first use and after very use**

1. As a fail-safe, please make an image of your robot's hard drive. You should always be able to restore this image if you need to revert back to your previous configuration.

   1. The easiest approach may be to either connect a removable (USB or similar) hard drive to the robot PC, or to unplug the robot hard drive and insert it into a PC or workstation.
   2. You can then use a tool such as [CloneZilla](http://clonezilla.org/show-live-doc-content.php?topic=clonezilla-live/doc/01_Save_disk_image) or [dd](https://wiki.archlinux.org/index.php/disk_cloning) to write a backup image of your robot's hard drive onto another hard drive.
   3. Alternatively, you can simply replace the robot computer's hard-drive, reserving the Hydro-configured drive and installing a new one to use with Indigo.

2. There are several places in the filesystem you should specifically look for customizations for your Husky:

   | **Location**:                        | **Description**:                                             |
   | ------------------------------------ | ------------------------------------------------------------ |
   | /etc/network/interfaces              | Your robot may have a custom network configuration configured in this file. |
   | /etc/ros/hydro/husky-core.d/*.launch | Will contain base.launch and description.launch, may contain custom launch files for your robot configuration. |
   | /etc/ros/setup.bash                  | May contain environment variables for your configuration.    |

## Communicating With HUSKY

To communicate directly with the robot PC, you can SSH in. It will be necessary to ssh into the robot for tasks such as installing, modifying or removing software and files on the robot. Note that you will not be able to use GUI tools such as rviz over an SSH connection:

```
ssh administrator@192.168.131.1
```

OR

```
ssh administrator@cpr-umb02
```

In order to use rviz and other visualization tools, you must declare the robot as master, and set the user computerIP. In a console on the user pc, type: 

```
export ROS_MASTER_URI=http://cpr-umb02:11311
```

You should then be able to view a list of topics published by the robot with:

```
rostopic list
```

It will be necessary to declare the robot as master in every new terminal window, unless you change the master permanently in your ROS environment variables. If you are unable to connect with the robot via its hostname, your computer or network equipment may not be routing hostnames properly. In Ubuntu on your local computer, open your /etc/hosts file:

```
sudo nano /etc/hosts
```

Add the following line immediately below the line that contains 127.0.1.1, substituting in the robot’s current
wifi IP address. This address may be obtained by connecting directly to the robot via Ethernet, and using the “ifconfig” command. You may want to talk to your system administrator about giving the robot a permanent wifi address to ensure it always connects with the same IP address. The below example shows the setting if wired directly into the robot Lan.

```
192.168.131.1 			cpr-umb02
```

To ease communications between the robot and your computer, you can also add a similar entry in the robot’s computer, pointing at one or more development computers.