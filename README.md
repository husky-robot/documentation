# HUSKY robot

This repository is to illustrate what is happening with the HUSKY robot project at the University of Melbourne.



## Contributors

| Name        | Student/Staff # | E-mail                        |
| ----------- | --------------- | ----------------------------- |
| Evan Thomas | 705935          | evant1@student.unimelb.edu.au |
|             |                 |                               |
|             |                 |                               |

## useful online documentation

### Clearpath videos:

* How to setup your husky: https://www.youtube.com/watch?v=wA8UTF0mKBY 

## Documentation

[LiDAR](LIDAR.md) (Evan)

