---

---

# LiDAR 

LiDAR model: [Velodyne HDL-32E](https://velodynelidar.com/hdl-32e.html)/[Manual](https://velodynelidar.com/lidar/products/manual/63-9113%20HDL-32E%20manual_Rev%20G.pdf)

This resource is primarily maintained by [Evan Thomas](evant1@student.unimelb.edu.au)

[TOC]

## Specifications from manual

Laser: 

* Class 1 - eye safe
* 905 nm wavelength
* Time of flight distance measurement
* Measurement range 70 m (1 m to 70 m)

**Sensor:**

* 32 laser/detector pairs
* +10.67 to -30.67 degrees field of view (vertical)
* 360 degree field of view (horizontal)
* 10 Hz frame rate
* Operating temperature, -10° C to +60° C
* Storage temperature, - 40° to 105° C
* Accuracy: <2 cm (one sigma at 25 m)
* Angular resolution (vertical) ~1.33°
* Angular resolution (horizontal) ~0.16° at 600 rpm

**Mechanical:**

* Power: 12V @ 2 Amps
* Operating voltage: 9-32 VDC
* Weight: <2 kg
* Shock: 500 m/sec2 amplitude, 11 msec duration
* Vibration: 5 Hz to 2000 Hz, 3 Grms
* Environmental Protection: IP67

**Output:**

* Approximately 700,000 points/second
* 100 Mbps Ethernet connection
* UDP packets
  * distance
  * rotation angle
* Orientation – internal MEMS accelerometers and gyros for six-axis motion correction – external correction
* GPS time-synchronized with included GPS receiver

**Dimensions (Height/Diameter):**

* Unit: 5.68” x 3.36” [149.86mm x 85.3mm]

**Sensor Weight:**

* 2.9 lbs [ 1.3 Kg]

**Shipping Weight (approx.):** 

* 14.35 lbs [ 6.5 Kg]

## Data extraction and sample data

There are many sources of sample data for the Velodyne scanners. The most likely filetype to find this data in is a PCAP dump. As the device streams data, this is effectively an Ethernet dump of two sets of data:

* The LiDAR stream (on port 2368)
* GPS positioning data (on port 8308)

This data can be extracted through many ways:

* [Wireshark](#Wireshark)
  * An understanding of the UDP packet data is needed and may be available in the documentation. Manual for the device is is linked above.
* [Veloview](#Veloview) ([Website](https://www.paraview.org/veloview/)/[Github](https://github.com/Kitware/VeloView))
  * Can be exported as a 'corrected' CSV file
  * Not entirely sure if this incorporates the GPS data at the same time.
* Various Python libraries
  * [scapy](#Scapy-(Python))
* [ROS](#ROS)
  * To stream out the data as if it were playing in the robot in real-time.



### Wireshark



### Veloview

Veloview can open a PCAP file, and output a .csv for each frame. This is a single-threaded process and took about 15 minutes to convert 1361 frames from the **HDL32-V2_Moterey Highway.pcap** file. Each csv file contains one frame, is roughly 8MB in size. A sample from this file, frame 0000:

| Points_m_XYZ:0    | Points_m_XYZ:1   | Points_m_XYZ:2    | X                 | Y                | Z                 | intensity | laser_id | azimuth | distance_m | adjustedtime | timestamp | vertical_angle |
| ----------------- | ---------------- | ----------------- | ----------------- | ---------------- | ----------------- | --------- | -------- | ------- | ---------- | ------------ | --------- | -------------- |
| 0.002740575233474 | 3.92558431625366 | -2.32806277275085 | 0.002740575303045 | 3.92558431979511 | -2.32806276492404 | 8         | 0        | 4       | 4.564      | 79597245052  | 397245052 | -30.67         |
| 0.003622930496931 | 4.15157127380371 | -2.33261322975159 | 0.003622930518913 | 4.15157151018959 | -2.33261331346552 | 5         | 2        | 5       | 4.762      | 79597245055  | 397245055 | -29.33         |
| 0.004613855853677 | 4.40590620040894 | -2.34266304969788 | 0.004613855736537 | 4.40590607255661 | -2.3426630983016  | 3         | 4        | 6       | 4.99       | 79597245057  | 397245057 | -28            |
| 0.01805005967617  | 17.2365322113037 | -2.01567935943604 | 0.018050060456034 | 17.2365317674517 | -2.01567924654322 | 2         | 5        | 6       | 17.354     | 79597245058  | 397245058 | -6.6700001     |
| 0.005716382525861 | 4.67892026901245 | -2.35018467903137 | 0.005716382362123 | 4.67892029250733 | -2.35018472025836 | 4         | 6        | 7       | 5.236      | 79597245059  | 397245059 | -26.67         |
| 0.021737920120359 | 17.7927207946777 | -1.65997886657715 | 0.021737919533218 | 17.7927203566356 | -1.65997884728112 | 2         | 7        | 7       | 17.87      | 79597245060  | 397245060 | -5.3299999     |
| 0.00690074916929  | 4.94229412078857 | -2.33938097953796 | 0.006900749197557 | 4.94229434447366 | -2.33938089935012 | 4         | 8        | 8       | 5.468      | 79597245061  | 397245061 | -25.33         |
| 0.008279911242425 | 5.27115058898926 | -2.34687042236328 | 0.008279911105051 | 5.2711507875695  | -2.34687043054737 | 4         | 10       | 9       | 5.77       | 79597245064  | 397245064 | -24            |
| 0.009843280538917 | 5.63977861404419 | -2.35570502281189 | 0.00984328026377  | 5.63977843019471 | -2.35570506815777 | 5         | 12       | 10      | 6.112      | 79597245066  | 397245066 | -22.67         |
| 0.011642172932625 | 6.06405973434448 | -2.36794090270996 | 0.011642173154843 | 6.06405969620575 | -2.3679409749099  | 3         | 14       | 11      | 6.51       | 79597245068  | 397245068 | -21.33         |
| 0.011727839708328 | 5.16888093948364 | -1.74655830860138 | 0.011727839494017 | 5.16888117484566 | -1.7465582893528  | 1         | 18       | 13      | 5.456      | 79597245073  | 397245073 | -18.67         |
| 0.10662904381752  | 43.6384429931641 | 2.03504371643066  | 0.106629046971085 | 43.6384443301759 | 2.03504365763611  | 22        | 19       | 14      | 43.686     | 79597245074  | 397245074 | 2.6700001      |
| 0.012842952273786 | 5.25603914260864 | -1.64009833335877 | 0.012842952205178 | 5.2560392384709  | -1.64009834529303 | 2         | 20       | 14      | 5.506      | 79597245075  | 397245075 | -17.33         |
| 0.113845460116863 | 43.4856643676758 | 3.04082417488098  | 0.1138454597546   | 43.4856630554828 | 3.04082420345391  | 22        | 21       | 15      | 43.592     | 79597245076  | 397245076 | 4              |
| 0.135436996817589 | 45.6467361450195 | 5.33805370330811  | 0.13543699261964  | 45.6467370326854 | 5.33805386727172  | 26        | 25       | 17      | 45.958     | 79597245081  | 397245081 | 6.6700001      |
| 0.016762606799603 | 5.64955139160156 | -1.33862709999085 | 0.016762606042836 | 5.64955153846841 | -1.33862706876407 | 1         | 26       | 17      | 5.806      | 79597245082  | 397245082 | -13.33         |
| 0.016443405300379 | 3.92555093765259 | -2.32806277275085 | 0.016443405067995 | 3.925550837399   | -2.32806276492404 | 10        | 0        | 24      | 4.564      | 79597245098  | 397245098 | -30.67         |
| 0.018167853355408 | 4.16373872756958 | -2.33947110176086 | 0.018167853265507 | 4.16373883560117 | -2.33947105945218 | 5         | 2        | 25      | 4.776      | 79597245101  | 397245101 | -29.33         |
| 0.019969269633293 | 4.40056562423706 | -2.33984637260437 | 0.019969269842883 | 4.4005654938951  | -2.33984626892488 | 3         | 4        | 26      | 4.984      | 79597245103  | 397245103 | -28            |

### ROS

A stream of the PCAP file can be started with ROS tools then saved by capturing the stream [(forum post)](http://www.pcl-users.org/Convert-pcap-to-pcd-Velodyne-HDL32E-td4034165.html)

**Start the stream in one terminal**:

``roslaunch velodyne_pointcloud 32e_points.launch pcap:=(saved PCAP file)``

**in a seperate terminal:**

``rosrun pcl_ros pointcloud_to_pcd input:=/velodyne_points _prefix:=(prefix of PCD files) ``

*Note: The paths of the files need to be absolute to work properly*



This will create a file for each frame in the capture. The files will output as PCD - a point cloud format read by [PCL](http://pointclouds.org/) and can be interpreted by [PDAL](https://pdal.io/).



As this is streaming the data, it may be possible to lose information at the beginning of the stream. Also, it seems that this plays out in real-time, so it's not the fastest method. After 6 minutes of streaming (before terminating) a 200mb PCAP file through this method, it produced 2,320 PCD files totalling 4.6 GiB. 

### Scapy (Python)

I have not explored this very much. It would be neat to be able to dump a PCAP file into Python, and have it convert the PCAP dump into both GNSS and LiDAR data. This would be useful further down the track to simplify steps.



## Working with the data

### Python

#### Visualise

PCD files can be imported into Python to visualise individually at this stage.

https://bitbucket.org/huskyuomgeomatics/lidar-python/

![Screenshot_20190130_192024](assets/Screenshot_20190130_192024.png)





### Import straight from pcap, visualise, extract points

Check the lidar-python repository's help file. One method that I've been working on is to parse the PCAP file directly to points and work on it from there. I've found an existing library that does mostly this.

![Screenshot_20190203_214244](assets/Screenshot_20190203_214244.png)

